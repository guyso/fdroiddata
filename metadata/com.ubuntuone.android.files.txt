AntiFeatures:NonFreeNet,UpstreamNonFree
Categories:Internet
License:AGPLv3
Web Site:https://one.ubuntu.com
Source Code:https://code.launchpad.net/ubuntuone-android-files
Issue Tracker:https://bugs.launchpad.net/ubuntuone-android-files

Auto Name:Ubuntu One Files
Summary:File synchronisation
Description:
Client for Ubuntu One, an online storage service provided with the
Ubuntu OS.

The proprietary Google Analytics library was removed from this build.

Anti-feature:NonFreeNet. The Ubuntu One Files server is proprietary software.
.

Repo Type:bzr
Repo:lp:ubuntuone-android-files

Build:1.2.7,460
    commit=460
    extlibs=android/android-support-v4.jar,oauth-signpost/signpost-core-1.2.1.1.jar,oauth-signpost/signpost-commonshttp4-1.2.1.1.jar
    srclibs=NoAnalytics@158a4a
    prebuild=sed -i 's@../../GreenDroid@Greendroid@g' build.xml && \
        sed -i 's@\(android.library.reference.1=\).*@\1Greendroid/GreenDroid/@' project.properties && \
        echo "android.library.reference.2=$$NoAnalytics$$" >> project.properties && \
        sed -i 's@executable="android"@executable="$$SDK$$/tools/android"@g' build.xml && \
        sed -i 's@android@$$SDK$$/tools/android@g' tools.sh && \
        sed -i '87,108d' build.xml && \
        ant setup

Build:1.2.8,472
    disable=Build fails
    commit=472
    target=android-10
    extlibs=android/android-support-v4.jar,oauth-signpost/signpost-core-1.2.1.1.jar,oauth-signpost/signpost-commonshttp4-1.2.1.1.jar
    srclibs=NoAnalytics@158a4a
    prebuild=sed -i 's@../../GreenDroid@Greendroid@g' build.xml && \
        sed -i 's@\(android.library.reference.1=\).*@\1Greendroid/GreenDroid/@' project.properties && \
        echo "android.library.reference.2=$$NoAnalytics$$" >> project.properties && \
        sed -i 's@executable="android"@executable="$$SDK$$/tools/android"@g' build.xml && \
        sed -i 's@android@$$SDK$$/tools/android@g' tools.sh && \
        sed -i '87,108d' build.xml && \
        ant setup

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.8
Current Version Code:472

