Categories:Office
License:GPLv2+
Web Site:http://forum.xda-developers.com/showthread.php?t=2330864
Source Code:https://github.com/DF1E/SimpleExplorer
Issue Tracker:https://github.com/DF1E/SimpleExplorer/issues

Auto Name:Simple Explorer
Summary:File manager
Description:
Simple file explorer inspired by [[com.cyanogenmod.filemanager.ics]] and [[com.docd.purefm]].
.

Repo Type:git
Repo:https://github.com/DF1E/SimpleExplorer

Build:2.0.5,53
    commit=22b79f6a346766f7c19bff47c8776a2839045b8f
    srclibs=RootTools@3.4,CommonsIO@2.4,JUnrar@junrar-0.7
    rm=libs/RootTools-3.4.jar,libs/commons-io-2.4.jar,libs/junrarlib.jar
    prebuild=pushd $$JUnrar$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JUnrar$$/target/junrar-0.7.jar libs/ && \
        pushd $$CommonsIO$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$CommonsIO$$/target/commons-io-2.4.jar libs/ && \
        cp -fR $$RootTools$$/RootTools/src/main/java/com  src/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.5
Current Version Code:53

