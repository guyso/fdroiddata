Disabled:No license given; issue ticket opened
Categories:Internet
License:
Web Site:http://twister.net.co
Source Code:https://github.com/miguelfreitas/twister-webview-app
Issue Tracker:https://github.com/miguelfreitas/twister-webview-app/issues

Auto Name:twister simpleclient
Summary:Microblogging client for Twister
Description:
Twister is a fully decentralized P2P microblogging platform
leveraging from the free software implementations of Bitcoin
and BitTorrent protocols. 
.

Repo Type:git
Repo:https://github.com/miguelfreitas/twister-webview-app.git

Build:1.0,21
    commit=96ad73c81320c3a086542d0bdad119bc21ed8df7

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:43

