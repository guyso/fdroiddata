Categories:Office
License:Apache2
Web Site:http://forum.xda-developers.com/showthread.php?t=2127834
Source Code:https://github.com/MohammadAdib/FloatingStickies
Issue Tracker:https://github.com/MohammadAdib/FloatingStickies/issues

Auto Name:Floating Stickies
Summary:Sticky notes in a mini-window
Description:
* Dock to the side & resize
* Copy/Paste/Share
* Smooth fun animations & colors
* Save the state of the stickies
* Simple & Clean look
* Stickies are auto-saved
.

Repo Type:git
Repo:https://github.com/MohammadAdib/FloatingStickies.git

Build:27.0,27
    commit=5a8478752b
    subdir=Floating Stickies
    update=.,../StandOutSDK
    rm=Floating Stickies.apk

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:27.0
Current Version Code:27

